export default {
    server: {
        port: 3000,
        host: '0.0.0.0'
    },
    head: {
        title: 'novoselie',
        htmlAttrs: {
            lang: 'en'
        },
        meta: [
            {charset: 'utf-8'},
            {name: 'viewport', content: 'width=device-width, initial-scale=1'},
            {hid: 'description', name: 'description', content: ''},
            {name: 'format-detection', content: 'telephone=no'}
        ],
        link: [
            {rel: 'icon', type: 'image/x-icon', href: '/favicon.ico'}
        ]
    },

    mode: 'universal',

    // Global CSS: https://go.nuxtjs.dev/config-css
    css: [],

    // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
    plugins: [
        {src: '~/plugins/lightGallery.client.js', ssr: false}
    ],

    // Auto import components: https://go.nuxtjs.dev/config-components
    components: true,

    // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
    buildModules: [],

    // Modules: https://go.nuxtjs.dev/config-modulesнфкт
    modules: [
        // https://go.nuxtjs.dev/axios
        '@nuxtjs/axios',
        ['nuxt-fontawesome', {
            component: 'fa',
            imports: [
                //import whole set
                {
                    //set: '@fortawesome/free-solid-svg-icons',
                    set: '@fortawesome/free-solid-svg-icons',
                    icons: ['fas']
                },
                {
                    set: '@fortawesome/free-regular-svg-icons',
                    icons: ['far']
                },
            ]
        }]
    ],

    fontawesome: {
        component: 'fa'
    },

    // Axios module configuration: https://go.nuxtjs.dev/config-axios
    axios: {},

    // Build Configuration: https://go.nuxtjs.dev/config-build
    build: {}
}
