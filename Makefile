include .env
export $(shell sed 's/=.*//' .env)

up:
	docker-compose up -d app app_static

down:
	docker-compose down

restart: down up

init:
	docker network create ${NETWORK}

install:
	docker-compose run app yarn install
